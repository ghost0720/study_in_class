package com.suanfa.class5;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;

public class Graph {

    private ArrayList<String> vertexList; //存储顶点集合
    private int[][] edges; //存储图对应的邻接矩阵
    private int numOfEdges;//边的数目
    //定义数组boolean[] ，记录某个节点是否被访问
    private boolean[] isVisited;


    public Graph() {

    }
    public Graph(int n) {
        //初始化
        edges = new int[n][n];
        vertexList = new ArrayList<>(n);
        numOfEdges = 0;
        isVisited = new boolean[5];
    }

    //插入节点
    public void insertVertex(String vertex) {
        vertexList.add(vertex);
    }

    //添加边
    /**
     *
     * @param v1 表示点的下标
     * @param v2 表示点的下标
     * @param weight 权重
     */
    public void insertEdge(int v1,int v2,int weight) {

        edges[v1][v2] = weight;
        edges[v2][v1] = weight;
        numOfEdges++;

    }


    //常用方法
    //返回节点个数
    public int getNumOfVertex() {
        return vertexList.size();
    }

    //得到边的数目
    public int getNumOfEdges() {
        return numOfEdges;
    }

    //返回节点i对应的数据
    public String getValueByIndex(int i) {
        return vertexList.get(i);
    }

    //返回v1和v2的权值
    public int getWeight(int v1,int v2) {
        return edges[v1][v2];
    }

    //显示图对应的矩阵
    public void showGraph() {

        for (int[] edge : edges) {
            System.out.println(Arrays.toString(edge));
        }

    }

    //根据前一个邻接节点的下标来获取下一个邻接节点的下标
    public int getNextNeighbor(int v1,int v2) {
        for (int i = v2+1; i < vertexList.size(); i++) {
            if (edges[v1][i]>0) {
                return i;
            }
        }
        return -1;
    }

    //得到一个邻接节点的下标
    //如果存在就返回对应的下标，否则返回-1
    public int getFirstNeighbor(int index) {
        for (int i = 0; i < vertexList.size(); i++) {
            if (edges[index][i]>0) {
                return i;
            }
        }
        return -1;
    }


    //深度优先遍历算法
    private void dfs(boolean[] isVisited,int i) {
        System.out.print(getValueByIndex(i) + "->");//输出
        //将该节点设置以访问
        isVisited[i] = true;
        int w = getFirstNeighbor(i);
        while(w != -1) {//说明有该节点
            if (!isVisited[w]) {
                dfs(isVisited,w);
            }
            //如果w节点已经被访问过
            w = getNextNeighbor(i,w);

        }
    }

    //对dfs进行重载，遍历所有节点，并进行dfs
    public void dfs() {
        //遍历所有节点进行dfs
        for (int i = 0; i < getNumOfVertex(); i++) {
            if (!isVisited[i]) {
                dfs(isVisited,i);
            }
        }
    }








    //广度优先遍历的方法
    private void bfs(boolean[] isVisited,int i) {
        int u;//表示队列的头节点对应的下标
        int w;//邻接节点W
        //队列，记录节点访问的顺序
        LinkedList queue = new LinkedList();
        //访问节点
        System.out.print(getValueByIndex(i) + "->");
        //标记已访问
        isVisited[i] = true;

        //将节点加入队列
        queue.addLast(i);

        while(!queue.isEmpty()) {
            //取出队列的头节点下标
            u = (Integer)queue.removeFirst();
            //得到第一个邻接节点下标W
            w = getFirstNeighbor(u);
            while(w != -1) {//找到了
                //是否访问过
                if (!isVisited[w]) {
                    System.out.print(getValueByIndex(w) + "->");
                    //标记已经访问
                    isVisited[w] = true;
                    //入队列
                    queue.addLast(w);
                }
                //如果已经访问过，以u为前驱节点，找w后面那个邻接节点
                w = getNextNeighbor(u,w);//找u这行的w的下一个节点
            }
        }

    }

    //遍历所有节点，进行广度优先搜索
    public void bfs() {
        for (int i = 0; i < getNumOfVertex(); i++) {
            if (!isVisited[i]) {
                bfs(isVisited,i);
            }
        }
    }








}
