package com.suanfa.class5;


/**
 * 1.访问初始节点V并标记节点V为已访问
 * 2.节点V入队列
 * 3.当队列非空时，继续执行，否则算法结束
 * 4.出队列，取得队头节点U
 * 5.查找节点U的第一个邻接节点W
 * 6.若节点U的邻接节点W不存在，则转到步骤3，否则循环执行以下3个步骤：
 *      6.1 若节点W尚未被访问，则访问节点W并标记为已访问
 *      6.2 节点W入队列
 *      6.3 查找节点U的继W邻接节点后的下一个邻接节点W，转到第六步
 */
public class BFS {

    public static void main(String[] args) {

        Graph graph = new Graph(5);
        String vertexValue[] = {"A","B","C","D","E"};

        //循环添加顶点
        for (String str:vertexValue) {
            graph.insertVertex(str);
        }
        //添加边
        graph.insertEdge(0,1,1);
        graph.insertEdge(0,2,1);
        graph.insertEdge(0,4,1);
        graph.insertEdge(1,2,1);
        graph.insertEdge(1,3,1);
        graph.insertEdge(1,4,1);

        graph.showGraph();

        //广度遍历
        System.out.println("广度遍历");
        graph.bfs();

    }

}
