package com.suanfa.class5;


/**
 * 深度优先遍历算法步骤
 * 1.访问初始节点V，并标记节点V已访问
 * 2.查找节点V的第一个邻接节点W
 * 3.若W存在，则继续执行4，如果W不存在，则回到第一步，将从V的下一个节点继续
 * 4.若W未被访问，对W进行深度优先遍历递归（即把W当作另一个V，然后进行步骤123）
 * 5.查找节点V的W邻接节点的下一个邻接节点，转到步骤3
 */
public class DFS {

    public static void main(String[] args) {

        Graph graph = new Graph(5);
        String vertexValue[] = {"A","B","C","D","E"};

        //循环添加顶点
        for (String str:vertexValue) {
            graph.insertVertex(str);
        }
        //添加边
        graph.insertEdge(0,1,1);
        graph.insertEdge(0,2,1);
        graph.insertEdge(1,2,1);
        graph.insertEdge(1,3,1);
        graph.insertEdge(1,4,1);

        graph.showGraph();

        //深度遍历
        System.out.println("深度遍历");
        graph.dfs();

    }

}
