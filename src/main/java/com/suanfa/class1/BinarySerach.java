package com.suanfa.class1;

import java.util.Arrays;
import java.util.HashMap;

public class BinarySerach {

    //在数组中查找target值，返回下标
    //有序数组
    public static int binarySerachByOrderArray(int[] arrs,int target) {

        int left = 0;
        int right = arrs.length-1;

        while(left<=right) {
            int mid = (left+right)/2;
            if (arrs[mid] == target) {
                return mid;
            }
            else if (arrs[mid] > target) {
                left = mid + 1;
            }
            else {
                right = mid - 1;
            }
        }

        return -1;

    }

    //无序数组
    public static int binarySerachByDisorderArray(int[] arrs,int target) {

        HashMap<Integer, Integer> map = new HashMap<>();

        for (int i = 0; i < arrs.length; i++) {
            map.put(arrs[i],i);
        }

        QuickSort.quickSort(arrs);
        return map.get(arrs[binarySerachByOrderArray(arrs,target)]);

    }



    public static void main(String[] args) {
//        int[] arrs = {1,2,3,4,5,6,7,8,9,12};
//        int[] arrs = {2,53,9,1,51,3,7,0,34,5,70,8};

        int[] arrs = {90,54,43,23,12,10,9,3};

        System.out.println(Arrays.toString(arrs));
        int i = 0;
        System.out.println(binarySerachByOrderArray(arrs,3));
//        System.out.println("数字" + i + "在索引：" + binarySerachByDisorderArray(arrs,i) + "的位置");
    }


}
