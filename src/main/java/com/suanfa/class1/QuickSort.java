package com.suanfa.class1;

import java.util.Arrays;

public class QuickSort {

    public static void quickSort(int[] arrs){
        int k = 0;
        int index = 0;//下标
        for (int i = 0; i < arrs.length; i++) {
            index = i;
            for (int j = i; j < arrs.length; j++) {
                if (arrs[j]<arrs[index]) {
                    index = j;
                }
            }

            //交换i下标和index下标值
            int arr = arrs[i];
            arrs[i] = arrs[index];
            arrs[index] = arr;
            k++;
        }
        System.out.println(k);
    }

    public static void main(String[] args) {

        int[] arrs = {3,5,8,1,9,4,9,2,3,5,6};
        System.out.print("排序前：");
        System.out.println(Arrays.toString(arrs));
        quickSort(arrs);
        System.out.print("排序后：");
        System.out.println(Arrays.toString(arrs));

    }

}
