package com.suanfa.class6;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.text.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.*;
import javax.swing.*;
import java.util.Date;
import java.text.SimpleDateFormat;

public class Pro extends JFrame{

    private JTextField textField = new JTextField(45);//系统时间文本框
    private JTextField textField2 = new JTextField(45);//倒计时文本框
    int x=1;
    double t1=0;
    private boolean TT = true;

    public  Pro()throws InterruptedException{
        setTitle("计时器");  //标题
        setLayout(null);  //取消布局管理器设置
        setBounds(100,120,450,450);
        Container c = getContentPane();  //容器对象
        JButton b1 = new JButton("开始");
        b1.setBounds(180,130,80,30);
        JButton b2 = new JButton("暂停");
        b2.setBounds(180,185,80,30);
        JButton b8 = new JButton("清零");
        b8.setBounds(180,240,80,30);
        c.add(b1);c.add(b2);c.add(b8);//增加控件
        setVisible(true);//窗口可视化
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);//结束窗口所在的应用程序

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式

        textField.setFont(new Font("宋体",Font.BOLD,60));//改变数字的大小
        textField.setBackground(Color.white);//改变文本框的颜色
        textField.setBounds(10,10,400,90);//改变文本框的大小
        textField.setEditable(false);//控件不能编辑
        add(textField);//增加文本框，显示系统时间
        textField.setText("   "+df.format(new Date()));//获系统时间

        textField2.setFont(new Font("宋代",Font.BOLD,60));
        textField2.setBackground(Color.white);
        textField2.setBounds(10,280,400,90);
        textField2.setEditable(false);
        add(textField2);//增加文本框，显示计时
        textField2.setText("   计时："+t1);

        //获取系统时间时动态显示,线程
        Display timedisplay = new Display();
        timedisplay.start() ;

        //添加一个action监听，确定键
        b1.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                //计时开始
                Dis tt = new Dis();
                tt.start();
            }
        });

        //添加一个action监听，暂停键
        b2.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                Dis tt2 = new Dis();
                tt2.sto();
            }
        });

        //添加一个action监听，清零键
        b8.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                textField2.setText("   计时："+"0");
                t1=0;
            }
        });
    }


    //显示系统时间
    private class Display extends Thread {
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");//系统时间以时：分：秒显示
        public Display() {

        }
        @Override
        public void run(){
            while(true){
                textField.setText("   "+sdf.format(new Date()));
                try {
                    Thread.sleep(1000) ;//迟顿一秒
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    //计时开始
    private class Dis extends Thread{
        public Dis(){

        }
        public void sto(){
            TT=false;
        }
        public void run(){
            TT=true;
            while(TT){
                t1 = t1 + 0.001;
                textField2.setText("   计时："+t1);
                try {
                    Thread.sleep(1) ;
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

    }



    public static void main(String[] args)throws InterruptedException {
        new Pro();
    }
}
