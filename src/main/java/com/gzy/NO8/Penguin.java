package com.gzy.NO8;

import com.gzy.NO8.Interface.Bird;

public class Penguin extends Bird {

    private Climate climate;

    public Penguin(Climate climate,String name) {
        super(name);
        this.climate = climate;
    }
    public Penguin(String name) {
        super(name);
        this.climate = new Climate("气候舒适");
    }
    public Penguin(){
        this.climate = new Climate();
    }

    @Override
    public void layEggs() {
        System.out.println("企鹅在下蛋");
    }

    @Override
    public void metabolism(Oxygen oxygen, Water water) {
        System.out.println("企鹅在新陈代谢");
    }

    @Override
    public void reproduction() {
        System.out.println("企鹅在繁殖");
    }

    public void setClimate(Climate climate) {
        this.climate = climate;
    }

    public Climate getClimate() {
        return climate;
    }
}
