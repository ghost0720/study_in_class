package com.gzy.NO8;

import com.gzy.NO8.Duck;

public class DonaldDuck extends Duck {

    public DonaldDuck(String name) {
        super(name);
        System.out.println("唐老鸭来了");
    }
    public DonaldDuck(){}

    public void speak() {
        System.out.println("唐老鸭在说人话，恐怖如斯");
    }

}
