package com.gzy.NO8;


import java.util.Arrays;

//雁群
public class WildGooses {

    private WildGoose[] wildGooses = null;

    public WildGooses(WildGoose[] wildGooses) {
        this.wildGooses = wildGooses;
    }
    public WildGooses() {}

    public void VFly() {
        System.out.println(Arrays.toString(wildGooses) + "在按V型飞行");
    }
    public void YFly() {
        System.out.println(Arrays.toString(wildGooses) + "在按一字型飞行");
    }

    @Override
    public String toString() {
        return "WildGooses{" +
                "wildGooses=" + Arrays.toString(wildGooses) +
                '}';
    }
}
