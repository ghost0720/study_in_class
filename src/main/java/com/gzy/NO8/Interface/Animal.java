package com.gzy.NO8.Interface;

import com.gzy.NO8.Oxygen;
import com.gzy.NO8.Water;

public abstract class Animal {

    public boolean life;
    public String name;

    public Animal(String name) {
        this.name = name;
    }

    public Animal() {
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    //新陈代谢
    //在方法形参中依赖了氧气和水
    public abstract void metabolism(Oxygen oxygen, Water water);

    public abstract void reproduction();

}
