package com.gzy.NO8.Interface;

import com.gzy.NO8.Wing;

public abstract class Bird extends Animal {

    public Wing wing;
    public boolean toothless;//有角质，没牙齿
    public Bird(String name)
    {
        super(name);
        wing=new Wing();
        //在鸟Bird类中，初始化时，实例化翅膀Wing,它们之间同时生成
        this.toothless = true;
    }
    public Bird(){}

    public abstract void layEggs();

}
