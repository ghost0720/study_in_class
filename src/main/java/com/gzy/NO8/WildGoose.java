package com.gzy.NO8;

import com.gzy.NO8.Interface.Bird;
import com.gzy.NO8.Interface.Flight;

public class WildGoose extends Bird implements Flight {

    public WildGoose(String name) {
        this.name = name;
    }
    public WildGoose() {}

    @Override
    public void layEggs() {
        System.out.println("大雁在下蛋");
    }

    @Override
    public void metabolism(Oxygen oxygen, Water water) {
        System.out.println("大雁在新陈代谢");
    }

    @Override
    public void reproduction() {
        System.out.println("大雁在繁殖");
    }

    @Override
    public void fly() {
        System.out.println("大雁在飞翔");
    }

    @Override
    public String toString() {
        return "WildGoose{" +
                "name='" + name + '\'' +
                '}';
    }
}
