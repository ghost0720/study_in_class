package com.gzy.NO8;

import com.gzy.NO8.Interface.Bird;

public class Duck extends Bird {

    public Duck(String name) {
        super(name);
    }
    public Duck(){}

    @Override
    public void metabolism(Oxygen oxygen, Water water) {
        System.out.println("鸭在呼吸和喝水");
    }

    @Override
    public void reproduction() {
        System.out.println("鸭子在繁殖");
    }

    @Override
    public void layEggs() {
        System.out.println("鸭子在下蛋");
    }
}
