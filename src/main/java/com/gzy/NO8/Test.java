package com.gzy.NO8;

import com.gzy.NO8.Interface.Bird;

public class Test {

    public static void main(String[] args) {

        System.out.println("-------------这里为大雁的操作--------------");
        Bird wildGoose = new WildGoose("大雁");
        wildGoose.layEggs();//生蛋
        wildGoose.metabolism(new Oxygen(wildGoose.getName()),new Water(wildGoose.getName()));//新陈代谢
        wildGoose.reproduction();//繁殖
        ((WildGoose) wildGoose).fly();

        WildGoose wildGoose1 = new WildGoose("大雁1");
        WildGoose wildGoose2 = new WildGoose("大雁2");
        WildGoose wildGoose3 = new WildGoose("大雁3");
        WildGoose[] wildGooses = {wildGoose1,wildGoose2,wildGoose3};

        WildGooses gooses = new WildGooses(wildGooses);
        gooses.VFly();
        gooses.YFly();

        System.out.println("-------------这里为唐老鸭的操作--------------");
        DonaldDuck donaldDuck = new DonaldDuck("唐老鸭");
        donaldDuck.speak();
        donaldDuck.layEggs();
        donaldDuck.metabolism(new Oxygen(donaldDuck.getName()),new Water(donaldDuck.getName()));

        System.out.println("-------------这里为企鹅的操作--------------");
        Penguin penguin = new Penguin(new Climate("气候变暖"), "企鹅大大");
        penguin.layEggs();
        penguin.reproduction();
        penguin.metabolism(new Oxygen(penguin.getName()),new Water(penguin.getName()));


    }

}
