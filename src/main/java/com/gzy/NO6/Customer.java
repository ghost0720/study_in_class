package com.gzy.NO6;

public class Customer implements Comparable<Customer> {

    private String cardID;
    private String name;
    private int level;
    private int score;
    private int age;
    private char gender;


    public Customer(){}

    public Customer(String cardID, String name, int level, int score, int age, char gender) {
        this.cardID = cardID;
        this.name = name;
        this.level = level;
        this.score = score;
        this.age = age;
        this.gender = gender;
    }

    public String getCardID() {
        return cardID;
    }

    public void setCardID(String cardID) {
        this.cardID = cardID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public char getGender() {
        return gender;
    }

    public void setGender(char gender) {
        this.gender = gender;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "cardID='" + cardID + '\'' +
                ", name='" + name + '\'' +
                ", level=" + level +
                ", score=" + score +
                ", age=" + age +
                ", gender=" + gender +
                '}';
    }

    @Override
    public int compareTo(Customer o) {

        if (this.level<o.level) {
            return -1;
        }
        if (this.level>o.level) {
            return 1;
        }
        return 0;
    }
}
