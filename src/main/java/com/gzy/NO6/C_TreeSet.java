package com.gzy.NO6;

import java.util.TreeSet;

public class C_TreeSet {

    static TreeSet<Customer> customers = new TreeSet<>();

    public static void main(String[] args) {

        C_TreeSet c_treeSet = new C_TreeSet();

        c_treeSet.init();
        c_treeSet.showList();

    }







    public void init() {
//        public Customer(String cardID, String name, int level, int score, int age, char gender)
        Customer c1 = new Customer("111", "111", 1, 111, 11, '1');
        Customer c2 = new Customer("222", "222", 2, 222, 22, '0');
        Customer c3 = new Customer("333", "333", 3, 333, 33, '1');
        Customer c4 = new Customer("444", "444", 4, 444, 44, '0');
        Customer c5 = new Customer("555", "555", 5, 555, 55, '1');

        customers.add(c1);
        customers.add(c2);
        customers.add(c3);
        customers.add(c4);
        customers.add(c5);



    }

    public void showList() {

        for (Customer customer:customers) {
            System.out.println(customer);
        }

    }

}
