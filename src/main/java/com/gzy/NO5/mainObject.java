package com.gzy.NO5;

import com.gzy.NO5.IPromotionStrategy.Children61PromotionStrategy;

import java.util.*;

public class mainObject {

    public static Books books = new Books();
    static Scanner scanner;

    public static void main(String[] args) {

        /**
         * Node假数据
         */
        Node cartoon = new Node("漫画书", "1", CategoryTree.root, null);
        Node novel = new Node("小说", "2", CategoryTree.root, null);
        Node fantasyNovel = new Node("玄幻小说", "3", novel, null);
        Node classicNovel = new Node("经典小说", "4", novel, null);


        /**
         * 假数据
         * (String name, String ISBN, String typeID, double moneyValue, int number, String description, String publisher, String author, String shopper) {
         */
        Book book1 = new Book("海贼王", "111111111111", "1",32,54,"sasdfasfd","asdf","a","asdf");
        Book book2 = new Book("火影忍者","22222222", "1", 51,54,"sasdfasfd","asdf","a","asdf");
        Book book3 = new Book("斗破苍穹","333333333", "3", 53,54,"sasdfasfd","asdf","a","asdf");
        Book book4 = new Book("斗罗大陆","44444444444", "3", 51,54,"sasdfasfd","asdf","a","asdf");
        Book book5 = new Book("边城","5555555555", "4", 59,54,"sasdfasfd","asdf","a","asdf");
        books.addBookToBooksList(book1);
        books.addBookToBooksList(book2);
        books.addBookToBooksList(book3);
        books.addBookToBooksList(book4);
        books.addBookToBooksList(book5);






        scanner = new Scanner(System.in);

        System.out.println("请登录");
        System.out.print("账号:");
        String name = scanner.nextLine();
        System.out.print("密码:");
        String password = scanner.nextLine();

        if (new User().login(name,password)) {
            System.out.println("登录成功");
        }


        boolean b = true;

        while(b) {
            showMenus();//展示菜单
            String s = scanner.nextLine();

            switch (s) {
                case "addBook":
                    books.addBook();break;
                case "showAllBooks":
                    books.showAllBooks();break;
                case "addNode":
                    addNode();break;
                case "buy":
                    buy();break;
                case "showAllNodes":
                    showAllNodes();break;
                case "addPromotionStrategy":
                    addPromotionStarategy();break;
                case "showAllBooksInNodeId":
                    showAllBooksInNodeId();break;
                default:
                    System.out.println("输入错误,请重新输入");break;
            }
        }

    }

    private static void showAllBooksInNodeId() {
        System.out.println("请输入节点id:");
        String id = scanner.nextLine();
        ArrayList<Node> books = new ArrayList<>();
        Node node = new Node();
        Node nodeById = node.findNodeById(id);
        node.getAllChilderen(nodeById,books);

        List<Book> booksList = Books.booksList;
        HashMap<String, Node> stringNodeHashMap = new HashMap<>();
        for (Node node1:books) {
            stringNodeHashMap.put(node1.getID(),node1);
        }
        for (Book book:booksList) {
              if (stringNodeHashMap.get(book.getTypeID())!=null) {
                  System.out.println(book);
              }
        }

    }

    private static void addPromotionStarategy() {

    }

    private static void showAllNodes() {
        List<Node> nodes=new ArrayList<>();
        new Node().getAllChilderen(CategoryTree.root,nodes);

//        for (Node node:nodes) {
//            if (node!=CategoryTree.root)
//                System.out.println(node);
//        }

        Node.displaytree(CategoryTree.root,0);

    }

    private static void buy() {
        new Children61PromotionStrategy("2",0.1);//设置打折类型和折扣，并修改打完折后书的价钱
        System.out.println("选择要购买的书:");
        books.showAllBooks();//展示所有书
        Books books = new Books();
        HashMap<String, Integer> map = books.chooseBooks();

        double nums = books.calculateThePrice(map);
        System.out.printf("要支付的价格为:%.2f",nums);
        System.out.println();//换行
    }

    private static void addNode() {
        //添加一个node
        /**
         *     private String name;
         *     private Node parent;
         *     private List<Node> childes;
         *     private String ID;
         */
        System.out.println("请输入节点名字:");
        String name = scanner.nextLine();
        System.out.println("请输入父节点id:");
        String parentId = scanner.nextLine();
        System.out.println("请输入本节点ID:");
        String id = scanner.nextLine();

        //通过父节点ID找父节点Node
        //从root节点开始
        Node parent = Node.findNodeById(parentId);
        if (parent==null) {
            System.out.println("父节点id输入错误");
            return ;
        }
        new Node(name, id, parent, null);
    }

    public static void showMenus() {
        System.out.println("addBook(添加图书)");
        System.out.println("showAllBooks(展示所有书籍)");
        System.out.println("addNode(添加一个类型)");
        System.out.println("buy(购买)");
        System.out.println("showAllNodes(展示所有类型)");
        System.out.println("showAllBooksInNodeId(展示所有该节点下所有书)");
    }

}
