package com.gzy.NO5.IPromotionStrategy;

import com.gzy.NO5.Book;
import com.gzy.NO5.Books;
import com.gzy.NO5.Node;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Children61PromotionStrategy implements IPromotionStrategy {

    private String disCountTypeId;
    private double disCount;

    @Override
    public void discount(Book book) {
        if (book.getTypeID()==disCountTypeId) {
            book.setSaleMoney(book.getMoneyValue()*disCount);
        }
    }

    public Children61PromotionStrategy(){}
    public Children61PromotionStrategy(String disCountTypeId, double disCount) {
        this.disCountTypeId = disCountTypeId;
        this.disCount = disCount;

        //找到该节点类型的所有子节点(包括该节点)
        Node parent = Node.findNodeById(disCountTypeId);
        ArrayList<Node> childes = new ArrayList<>();
        parent.getAllChilderen(parent,childes);

        //将所有节点以id为键，节点为值做成map
        Map<String, Node> map = new HashMap<>();
        for (Node node:childes) {
            map.put(node.getID(),node);
        }


        //遍历将booksList中所有符合打折类型的书的打折后的价格修改
        for (Book book:Books.booksList) {
            if (map.get(book.getTypeID())!=null) {
                book.setSaleMoney(book.getMoneyValue() * disCount);
            }
        }

        System.out.println("-------------------------------------------当前折扣为:给"+parent.getName()+"类及其子类的打折,折扣为:" + (disCount*10)+"折-------------------------------------------------");

    }

    public String getDisCountTypeId() {
        return disCountTypeId;
    }

    public void setDisCountTypeId(String disCountTypeId) {
        this.disCountTypeId = disCountTypeId;
    }

    public double getDisCount() {
        return disCount;
    }

    public void setDisCount(double disCount) {
        this.disCount = disCount;
    }
}
