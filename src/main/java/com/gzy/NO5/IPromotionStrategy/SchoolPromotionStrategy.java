package com.gzy.NO5.IPromotionStrategy;

import com.gzy.NO5.Book;

public class SchoolPromotionStrategy implements IPromotionStrategy {

    private String disCountTypeId;
    private double disCount;

    @Override
    public void discount(Book book) {
        if (book.getTypeID()==disCountTypeId) {
            book.setSaleMoney(book.getMoneyValue()*disCount);
        }
    }

    public SchoolPromotionStrategy(){}

    public SchoolPromotionStrategy(String disCountTypeId, double disCount) {
        this.disCountTypeId = disCountTypeId;
        this.disCount = disCount;
    }

    public String getDisCountTypeId() {
        return disCountTypeId;
    }

    public void setDisCountTypeId(String disCountTypeId) {
        this.disCountTypeId = disCountTypeId;
    }

    public double getDisCount() {
        return disCount;
    }

    public void setDisCount(double disCount) {
        this.disCount = disCount;
    }
}
