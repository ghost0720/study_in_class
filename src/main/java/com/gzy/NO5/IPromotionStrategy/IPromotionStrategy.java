package com.gzy.NO5.IPromotionStrategy;

import com.gzy.NO5.Book;
import com.gzy.NO5.Node;

public interface IPromotionStrategy {

    //此处有个方法
    //计算书本折后售价方法
    void discount(Book book);

}
