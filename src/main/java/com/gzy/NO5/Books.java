package com.gzy.NO5;

import com.gzy.NO5.IOrderPromotionStrategy.I_300_100OrderPromotionStrategy;
import com.gzy.NO5.IPromotionStrategy.Children61PromotionStrategy;

import java.util.*;

public class Books {

    public static List<Book> booksList = new ArrayList<>();
    Scanner scanner = new Scanner(System.in);


    public void addBookToBooksList(Book book) {
        book.setSaleMoney(book.getMoneyValue());
        booksList.add(book);
        System.out.println("添加书到List成功");
    }



    public List<Book> getBooksList() {
        return booksList;
    }

    public void setBooksList(List<Book> booksList) {
        this.booksList = booksList;
    }

    /*
        通过用户输入来添加书籍到books
     */
    public void addBook() {

        /*private String name;√
        private String ISBN;
        private Node categoryType;√
        private String TypeID;√
        private double MoneyValue;√
        private double saleMoney;
        private int number;
        private String description;
        private String publisher;
        private String Author;
        private String shopper;*/
        //数据量填写太多不好测试，先只填写打勾的部分


        //输入书名
        System.out.print("书名:");
        String bookName = scanner.nextLine();
        //输入对应的类型NodeID
        System.out.print("输入书的类型（NodeID）:");
        String nodeID = scanner.nextLine();
        //输入这本书的价格
        System.out.print("输入这本书的价格:");
        int MoneyValue = scanner.nextInt();
        scanner.nextLine();
        //输入ISBN
        System.out.print("输入ISBN:");
        String isbn = scanner.nextLine();
        //输入书本剩余数量
        System.out.print("输入书本剩余数量:");
        int number = scanner.nextInt();
        scanner.nextLine();
        //输入书本描写
        System.out.print("输入书本描写:");
        String description = scanner.nextLine();
        //输入书本出版社
        System.out.print("输入书本出版社:");
        String publisher = scanner.nextLine();
        //输入书本作者
        System.out.print("输入书本作者:");
        String author = scanner.nextLine();
        //输入书店名称
        System.out.print("输入书店名称:");
        String shopper = scanner.nextLine();

        //将书添加到List存放书的集合中
        //(String name, String ISBN, Node categoryType, String typeID, double moneyValue, double saleMoney, int number, String description, String publisher, String author, String shopper)
        new Books().addBookToBooksList(new Book(bookName,isbn,nodeID,MoneyValue,number,description,publisher,author,shopper));

    }

    public void showAllBooks() {
        for (Book book:booksList) {
            System.out.println(book);
        }
    }

    public HashMap<String,Integer> chooseBooks() {

        HashMap<String, Integer> map = new HashMap<>();
        Map<String, Book> booksMap = getBooksMap();
        while(true) {
            System.out.print("请输入书名（如果不买了请直接回车）:");
            String name = scanner.nextLine();
            name = name.trim();
            if (name.equals("")) {
                break;
            }
            System.out.print("请输入购买的数量:");
            int num = scanner.nextInt();
            scanner.nextLine();
            int number = booksMap.get(name).getNumber();//得到这本书的数量
            while(num>number){
                System.out.println("你要购买的《" + name + "》数量不够,剩下 "+ number +"本,请重新选择数量:");
                num = scanner.nextInt();
                scanner.nextLine();
            }

            for (Book book:booksList) {
                if (book.getName().equals(name)) {
                    book.setNumber(number - num);
                }
            }

            map.put(name,num);
        }

        return map;

    }

    public double calculateThePrice(HashMap<String, Integer> map) {

        Set<String> keySet = map.keySet();
        Map<String, Book> booksMap = getBooksMap();


        double nums = 0;
        for (String key:keySet) {
            Integer integer = map.get(key);//书的数量
            double saleMoney = booksMap.get(key).getSaleMoney();
            nums += saleMoney * integer;
        }


        //是否满足满300 减 100 的折扣
        nums = new I_300_100OrderPromotionStrategy(300, 100).discountByOrder(nums);

        return nums;
    }

    public Map<String,Book> getBooksMap() {
        Map<String, Book> booksMap = new HashMap<>();
        for (Book book:booksList) {
            booksMap.put(book.getName(),book);
        }
        return booksMap;
    }



}
