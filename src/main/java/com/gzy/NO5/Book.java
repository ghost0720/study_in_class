package com.gzy.NO5;

public class Book {

    private String name;
    private String ISBN;
    private Node categoryType;
    private String TypeID;
    private double MoneyValue;
    private double saleMoney;
    private int number;
    private String description;
    private String publisher;
    private String Author;
    private String shopper;


    public Book(String name, String ISBN, String typeID, double moneyValue, int number, String description, String publisher, String author, String shopper) {
        this.name = name;
        this.ISBN = ISBN;
        this.categoryType = Node.findNodeById(typeID);
        TypeID = typeID;
        MoneyValue = moneyValue;
        this.saleMoney = moneyValue;
        this.number = number;
        this.description = description;
        this.publisher = publisher;
        Author = author;
        this.shopper = shopper;
    }

    public Book() {
    }

    public Book(String name, String TypeID, int MoneyValue) {
        this.name = name;
        this.TypeID = TypeID;
        this.MoneyValue = MoneyValue;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getISBN() {
        return ISBN;
    }

    public void setISBN(String ISBN) {
        this.ISBN = ISBN;
    }

    public Node getCategoryType() {
        return categoryType;
    }

    public void setCategoryType(Node categoryType) {
        this.categoryType = categoryType;
    }

    public String getTypeID() {
        return TypeID;
    }

    public void setTypeID(String typeID) {
        TypeID = typeID;
    }

    public double getMoneyValue() {
        return MoneyValue;
    }

    public void setMoneyValue(double moneyValue) {
        MoneyValue = moneyValue;
    }

    public double getSaleMoney() {
        return saleMoney;
    }

    public void setSaleMoney(double saleMoney) {
        this.saleMoney = saleMoney;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getAuthor() {
        return Author;
    }

    public void setAuthor(String author) {
        Author = author;
    }

    public String getShopper() {
        return shopper;
    }

    public void setShopper(String shopper) {
        this.shopper = shopper;
    }

    @Override
    public String toString() {
        return "Book{" +
                "name='" + name + '\'' +
                ", MoneyValue=" + MoneyValue +
                ", saleMoney=" + saleMoney +
                ", ISBN='" + ISBN + '\'' +
                ", categoryType=" + categoryType +
                ", TypeID='" + TypeID + '\'' +
                ", number=" + number +
                ", description='" + description + '\'' +
                ", publisher='" + publisher + '\'' +
                ", Author='" + Author + '\'' +
                ", shopper='" + shopper + '\'' +
                '}';
    }


}
