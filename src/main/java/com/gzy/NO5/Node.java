package com.gzy.NO5;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class Node {

    private String name;
    private Node parent;
    private List<Node> childes;
    private String ID;






    public Node getParentNode() {
        return null;
    }

    public List getChildsNode() {
        return null;
    }


    public void setParentNode(Node p) {

    }

    public void addChildNode(Node p) {
        //得到当前节点的所有子节点
        List<Node> childes = this.getChildes();
        if (childes==null) {
            //如果当前节点还是叶子节点
            //创建一个ArrayList
            childes = new ArrayList<>();
        }
        //将传入的节点P加入到childes中
        childes.add(p);
        this.setChildes(childes);
    }

    public void addChilds(List set) {

    }

    public List getAllChilds() {
        return null;
    }

    public Node(){}

    public Node(String name,String id) {
        this.setName(name);
        this.setID(id);
    }

    public Node(String name,String id,Node p,List childes) {
        this.name = name;
        this.ID = id;
        this.parent = p;
        this.childes = childes;

        //将该节点加入到父节点的子节点上
        addChildToParentNode(p,this);

    }

    public void addChildToParentNode(Node parent,Node child) {
        parent.addChildNode(child);
    }

    public Node(String name,String id,Node p) {

    }


    public static Node findNodeById(String parentId) {

        //遍历节点树
        //从根节点开始找
        Node node = CategoryTree.root.findNode(parentId);
        if (node==null) {
            System.out.println("没有找到该节点");
        }

        return node;

    }

    private Node findNode(String parentId) {
        if (this.getID().equals(parentId)) {
            return this;
        }else {
            Node node1;
            //得到当前节点的子节点
            List<Node> childes = this.getChildes();
            if (childes!=null) {

                for (Node node:childes) {
                    node1 = node.findNode(parentId);
                    if (node1==null) {
                        continue;
                    }else {
                        return node1;
                    }
                }
            }
        }
        return null;
    }


    public void getAllChilderen(Node node,List<Node> nodes){
        nodes.add(node);
        if(node.getChildes() != null){
            for(Node mNode:node.getChildes()){
                getAllChilderen(mNode,nodes);
            }
        }
    }


    public static void displaytree(Node f, int level) {       //递归显示树

        String preStr = "";
        for(int i=0; i<level; i++) {
            preStr += "    ";
        }

        for(int i=0; i<f.getChildes().size(); i++) {
            Node t = f.getChildes().get(i);
            System.out.println(preStr + "-"+t.getName() + "(" + t.getID() + ")");

            if(! (t.getChildes()==null)) {
                displaytree(t, level + 1);
            }
        }
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Node getParent() {
        return parent;
    }

    public void setParent(Node parent) {
        this.parent = parent;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }


    public List<Node> getChildes() {
        return childes;
    }

    public void setChildes(List<Node> childes) {
        this.childes = childes;
    }

    @Override
    public String toString() {
        return "Node{" +
                "name='" + name + '\'' +
                ", parentID=" + parent.getID() +
                ", ID='" + ID + '\'' +
                '}';
    }
}
