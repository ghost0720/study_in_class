package com.gzy.NO5.IOrderPromotionStrategy;

public interface IOrderPromotionStrategy {

    //此处定义一个方法
    //购书打折方法
    double discountByOrder(double values);

}
