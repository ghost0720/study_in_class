package com.gzy.NO5.IOrderPromotionStrategy;

public class I_300_100OrderPromotionStrategy implements IOrderPromotionStrategy {

    //满 value 的价格 减 relief的价钱
    private double value;
    private double relief;

    //此类是满300减一百的折扣
    @Override
    public double discountByOrder(double values) {

        //传入最后的价格来减免
        if (values>value) {

            System.out.printf("你满足 满%.2f减%.2f 的折扣",value,relief);
            System.out.println();
            System.out.println("原来价格为: " + values);
            System.out.println("现在价格为: " + (values - relief));

            return values - relief;
        }

        return values;
    }

    public I_300_100OrderPromotionStrategy(){}

    public I_300_100OrderPromotionStrategy(double value,double relief){
        this.value = value;
        this.relief = relief;

    }


}
