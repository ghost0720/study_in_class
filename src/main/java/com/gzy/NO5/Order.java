package com.gzy.NO5;

import java.time.LocalDate;
import java.util.Set;

public class Order {

    private String OrderID;
    private String userID;
    private String userName;
    private Set orderedBooks;
    private LocalDate orderedDate;


    //在此处定义一个方法
    //参加折扣活动PromotionStrategy（促销策略）






    public String getOrderID() {
        return OrderID;
    }

    public void setOrderID(String orderID) {
        OrderID = orderID;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Set getOrderedBooks() {
        return orderedBooks;
    }

    public void setOrderedBooks(Set orderedBooks) {
        this.orderedBooks = orderedBooks;
    }

    public LocalDate getOrderedDate() {
        return orderedDate;
    }

    public void setOrderedDate(LocalDate orderedDate) {
        this.orderedDate = orderedDate;
    }
}
