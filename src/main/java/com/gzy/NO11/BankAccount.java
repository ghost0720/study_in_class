package com.gzy.NO11;



public class BankAccount {

    private String name;
    private double balance;
    private int year;
    private double  rate = 0.03;

    public BankAccount() {
        this.balance = 10;
        this.year = 1;
    }

    public BankAccount(String name, double balance, int year) {
        this.name = name;
        this.balance = balance;
        this.year = year;
    }

    public void save(double money) {
        this.balance += money;
    }

    public void fetch(double money) {
        this.balance -= money;
    }

    public void calcTotal() {
        for (int i=0;i<this.year;i++) {
            this.balance = this.balance * (1+this.rate);
        }
        System.out.println("当前余额为："+this.balance);
    }

    public static void main(String[] args) {
        BankAccount bankAccount = new BankAccount("fans",1000,3);

        bankAccount.save(2000);
        bankAccount.calcTotal();


    }

}


