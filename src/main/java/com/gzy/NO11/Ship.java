package com.gzy.NO11;

import java.util.HashMap;
import java.util.Map;

class Container {

    public String regNumber;
    public double weight;
    public String description;

    public Container(String regNumber, double weight, String description) {
        this.regNumber = regNumber;
        this.weight = weight;
        this.description = description;
    }

    public Container() {
    }

    @Override
    public String toString() {
        return "Container{" +
                "regNumber='" + regNumber + '\'' +
                ", weight=" + weight +
                ", description='" + description + '\'' +
                '}';
    }
}


class DangerousContainer extends Container {

    public DangerousContainer(String regNumber, double weight, String description, int hazadousLevel) {
        super(regNumber, weight, description);
        this.hazadousLevel = hazadousLevel;
    }

    public DangerousContainer(int hazadousLevel) {
        this.hazadousLevel = hazadousLevel;
    }

    public DangerousContainer() {}

    public int hazadousLevel;

    @Override
    public String toString() {
        return "DangerousContainer{" +
                "regNumber='" + regNumber + '\'' +
                ", weight=" + weight +
                ", description='" + description + '\'' +
                ", hazadousLevel=" + hazadousLevel +
                '}';
    }
}



public class Ship {
    private String name;
    private double loadWeight;
    private Map<String,Container> map;
    private double currentLoadWeight;

    public Ship(String name, double loadWeight, Map<String, Container> map) {
        this.name = name;
        this.loadWeight = loadWeight;
        this.map = map;
        this.currentLoadWeight = 0;
    }

    public Ship() {
    }

    public boolean load(Container cnt) {

        if(cnt.weight>(this.loadWeight-this.currentLoadWeight)) {
            System.out.println("此集装箱因超重加载失败");
            return false;
        }

        this.currentLoadWeight += cnt.weight;
        this.map.put(cnt.regNumber,cnt);
        return true;
    }

    public boolean unload(String regNumber) {

        if (map.get(regNumber)!=null) {
            this.currentLoadWeight -= map.get(regNumber).weight;
            map.remove(regNumber);
            System.out.println("卸载编号为：" + regNumber + "的集装箱成功");
            return true;
        }

        return false;

    }

    @Override
    public String toString() {
        return "Ship{" +
                "name='" + name + '\'' +
                ", loadWeight=" + loadWeight +
                ", map=" + map +
                ", currentLoadWeight=" + currentLoadWeight +
                '}';
    }

    public static void main(String[] args) {

        Ship ship = new Ship("gzy",100,new HashMap<>());
        System.out.println(ship.load(new Container("1", 50, "1")));
        System.out.println(ship.load(new Container("2", 50, "2")));
        System.out.println(ship.load(new Container("3", 1, "3")));
        System.out.println(ship.toString());
    }


}
