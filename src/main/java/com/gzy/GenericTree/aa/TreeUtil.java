package com.gzy.GenericTree.aa;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TreeUtil {


    /**
     * 使用递归方法建树
     *
     * @param modules
     * @return
     */
    public static <T> List<Tree<T>> buildByRecursive(List<Tree<T>> modules, String pId) {

        List<Tree<T>> trees = new ArrayList<>();
        for (Tree<T> treeNode : modules) {

            if (pId.equals(treeNode.getParentId())) {

                trees.add(findChildren(treeNode, modules));
            }
        }

        return trees;
    }

    /**
     * 递归查找子节点
     *
     * @param
     * @return
     */
    public static <T> Tree<T> findChildren(Tree<T> treeNode, List<Tree<T>> nodes) {

        for (Tree<T> it : nodes) {

            if (treeNode.getId().equals(it.getParentId())) {

                if (treeNode.getChildren() == null) {

                    treeNode.setChildren(new ArrayList<>());
                }
                treeNode.getChildren().add(findChildren(it, nodes));
            }
        }
        return treeNode;
    }
}
