package com.gzy.GenericTree.aa;

import java.util.ArrayList;
import java.util.List;




    public class Tree<T> {

        /**
         * 节点ID
         */
        private String id;
        /**
         * 节点的子节点
         */
        private List<Tree<T>> children = new ArrayList<>();

        /**
         * 父ID
         */
        private String parentId;
        /**
         * 显示节点文本
         */
        private String text;


        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public List<Tree<T>> getChildren() {
            return children;
        }

        public void setChildren(List<Tree<T>> children) {
            this.children = children;
        }

        public String getParentId() {
            return parentId;
        }

        public void setParentId(String parentId) {
            this.parentId = parentId;
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }
    }
