package com.gzy.GenericTree;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class TreeEntity<T> implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -1303781676719137068L;
    /**
     *
     */
    private T model;
    public T getModel() {
        return model;
    }
    public void setModel(T model) {
        this.model = model;
    }
    public List<TreeEntity<T>> getChildren() {
        return children;
    }
    public void setChildren(List<TreeEntity<T>> children) {
        if(this.children ==null){
            this.children=new ArrayList<TreeEntity<T>>();
        }
        this.children = children;
    }
    private List<TreeEntity<T>> children;
}