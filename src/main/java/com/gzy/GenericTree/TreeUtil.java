package com.gzy.GenericTree;


import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.gzy.GenericTree.TreeEntity;

/**
 * 获取树形结构
 *
 * @author dypeng
 *
 */
public class TreeUtil<T> {
    private String getidMethod;
    private String getParentidMethod;

    public TreeUtil(String GetidMethod,String GetParentidMethod) {
        this.getidMethod = GetidMethod;
        this.getParentidMethod=GetParentidMethod;
    }

    /**
     * 向下获取树结构
     *
     * @param
     * @return
     * @throws Exception
     */
    public List<T> createTree(Set<T> setall) throws Exception {
        List<T> list = new ArrayList<T>();
        Map<Long, Set<T>> listhash = new HashMap<Long, Set<T>>();
        if (setall == null || setall.size() == 0) {
            return null;
        }

        for (T item : setall) {
            Long parentid = getParentid(item);
            if (listhash.containsKey(parentid)) {
                listhash.get(parentid).add(item);
            } else {
                Set<T> t = new HashSet<T>();
                t.add(item);
                listhash.put(parentid, t);
            }
        }
        Iterator<T> iterator = listhash.get(0l).iterator();
        while (iterator.hasNext()) {
            T item = iterator.next();
            list.add(item);
            getlow(item, listhash, list);
        }
        return list;
    }

    private void getlow(T parent, Map<Long, Set<T>> list, List<T> getlist) throws Exception {
        Long parentid = getid(parent);
        if (list.containsKey(parentid)) {
            Set<T> lp = list.get(parentid);
            for (T item : lp) {
                getlist.add(item);
                getlow(item, list, getlist);
            }
        }
        return;
    }

    public List<TreeEntity<T>> createTreeAsTreeEntity(Set<T> setall) throws Exception {
        Map<Long, Set<TreeEntity<T>>> listhash = new LinkedHashMap<Long, Set<TreeEntity<T>>>();
        if (setall == null || setall.size() == 0) {
            return null;
        }

        for (T item : setall) {
            Long parentid = getParentid(item);
            if (listhash.containsKey(parentid)) {
                TreeEntity<T> et =new TreeEntity<T>();
                et.setModel(item);
                listhash.get(parentid).add(et);
            } else {
                Set<TreeEntity<T>> t = new LinkedHashSet<TreeEntity<T>>();
                TreeEntity<T> et =new TreeEntity<T>();
                et.setModel(item);
                t.add(et);
                listhash.put(parentid, t);
            }
        }
        Iterator<TreeEntity<T>> iterator = listhash.get(0l).iterator();
        while (iterator.hasNext()) {
            TreeEntity<T> entitys = iterator.next();
            getlowasTreeEntity(entitys, listhash);
        }
        return new ArrayList(listhash.get(0l));
    }
    private void getlowasTreeEntity(TreeEntity<T> parent, Map<Long, Set<TreeEntity<T>>> list) throws Exception {
        Long id = getid(parent.getModel());
        if (list.containsKey(id)) {
            if(list.get(id)!=null){
                parent.setChildren(new ArrayList(list.get(id)));
                for(TreeEntity<T> t:parent.getChildren()){
                    getlowasTreeEntity(t,list);
                }
            }
        }
    }
    /**
     * 获取父级id
     *
     * @param item
     * @return
     * @throws Exception
     */
    private Long getid(T item) throws Exception {
        Method method = item.getClass().getMethod(getidMethod, null);
        Object invoke = method.invoke(item, null);
        Long id = 0l;
        if (invoke != null) {
            id = Long.parseLong(invoke.toString());
        }
        return id;
    }
    /**
     * 获取父级id
     *
     * @param item
     * @return
     * @throws Exception
     */
    private Long getParentid(T item) throws Exception {
        Method method = item.getClass().getMethod(getParentidMethod, null);
        Object invoke = method.invoke(item, null);
        Long parentid = 0l;
        if (invoke != null) {
            parentid = Long.parseLong(invoke.toString());
        }
        return parentid;
    }
}
