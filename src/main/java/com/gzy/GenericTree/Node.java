package com.gzy.GenericTree;

import java.util.List;

public class Node<T> {

    private T data;
    private  Node<T> parent;
    private List<Node<T>> childes;

    public Node(T data) {
        this.data = data;
    }

    public Node(T data, Node<T> parent, List<Node<T>> childes) {
        this.data = data;
        this.parent = parent;
        this.childes = childes;
    }

    public Node() {
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public Node<T> getParent() {
        return parent;
    }

    public void setParent(Node<T> parent) {
        this.parent = parent;
    }

    public List<Node<T>> getChildes() {
        return childes;
    }

    public void setChildes(List<Node<T>> childes) {
        this.childes = childes;
    }
}
