package com.gzy.NO10;

public interface NotifyObserver {

    public abstract void update(Observable observable,Object event);

}
