package com.gzy.NO10;

public class Plain implements Observable {

    private String state;//草场草的状态
    private String climate;//气候变化

    @Override
    public void addObserver(NotifyObserver obj) {

    }

    @Override
    public void notify(Object event) {

    }


    public String getClimate() {
        return climate;
    }

    public void setClimate(String climate) {
        this.climate = climate;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }


}
