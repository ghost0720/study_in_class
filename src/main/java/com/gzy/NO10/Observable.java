package com.gzy.NO10;

public interface Observable {

    public abstract void addObserver(NotifyObserver obj);
    public abstract  void notify(Object event);

}
