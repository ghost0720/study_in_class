package com.gzy.NO10.animal;

import com.gzy.NO10.NotifyObserver;
import com.gzy.NO10.Observable;

public class Snake implements NotifyObserver {

    private String name;

    public Snake(String name) {
        this.name = name;
    }

    public void eat() {
        System.out.println("蛇" + name + "吃牛");
    }

    @Override
    public void update(Observable observable, Object event) {

    }
}
