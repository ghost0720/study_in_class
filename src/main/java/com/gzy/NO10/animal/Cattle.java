package com.gzy.NO10.animal;

import com.gzy.NO10.NotifyObserver;
import com.gzy.NO10.Observable;

public class Cattle implements NotifyObserver {

    private String name;

    public Cattle(String name) {
        this.name = name;
    }

    public void eat() {
        System.out.println("牛" + name + "吃草");
    }

    @Override
    public void update(Observable observable, Object event) {

    }
}
