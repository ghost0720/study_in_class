package com.gzy.NO10.animal;

import com.gzy.NO10.NotifyObserver;
import com.gzy.NO10.Observable;

public class Tiger implements NotifyObserver {

    private String name;

    public Tiger(String name) {
        this.name = name;
    }

    public void eat() {
        System.out.println("老虎" + name + "吃牛");
    }

    @Override
    public void update(Observable observable, Object event) {

    }
}
