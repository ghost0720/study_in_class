package com.gzy.NO9;

import javax.swing.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class TriangleActionEvent implements ActionListener {

    Triangle triangle;

    public TriangleActionEvent(Triangle triangle) {
        this.triangle = triangle;
    }


    @Override
    public void actionPerformed(ActionEvent e) {
        // TODO Auto-generated method stub
        if (e.getSource()==triangle.exitButton) {
            clear();
        }


        if (e.getSource()==triangle.loginButton) {

            String text = triangle.jTextField1.getText();
            String text1 = triangle.jTextField2.getText();
            String text2 = triangle.jTextField3.getText();

            Integer integer = Integer.valueOf(text);
            Integer integer1 = Integer.valueOf(text1);
            Integer integer2 = Integer.valueOf(text2);

            boolean b1 = (integer + integer1)>integer2;
            boolean b2 = (integer + integer2)>integer1;
            boolean b3 = (integer2 + integer1)>integer;

            if (!(b1&&b2&&b3)) {
                JOptionPane.showMessageDialog(null,"边长出错","消息提示",JOptionPane.ERROR_MESSAGE);
                clear();
                return ;
            }



            //S=√p(p-a)(p-b)(p-c)，其中p=（a+b+c）
            int p = (integer + integer1 + integer2)/2;
            int a = p*(p-integer)*(p-integer1)*(p-integer2);
            double s = Math.sqrt(Double.valueOf(a));
            triangle.label4.setText(String.valueOf(s));
//            JOptionPane.showMessageDialog(null,s,"消息提示",JOptionPane.NO_OPTION);
            clear();
            /*if (jTextField.getText().contains("abc") && passwordField.getText().contains("123")) {//如果文本框包含abc并且密码框包含123则登录成功，否则登录失败
//                JOptionPane.showMessageDialog(null,"登录成功！" );

            }else {
                JOptionPane.showMessageDialog(null, "用户名或密码错误！");
            }
            if (e.getSource()==exitButton) {
                System.exit(0);
            }*/
        }
    }

    public void clear() {
        triangle.jTextField1.setText("");
        triangle.jTextField2.setText("");
        triangle.jTextField3.setText("");
    }

}
