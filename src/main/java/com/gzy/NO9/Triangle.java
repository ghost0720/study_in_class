package com.gzy.NO9;

import java.awt.FlowLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

@SuppressWarnings("all")
class Triangle extends JFrame {

    JPanel panel;
    JLabel label,label2,label3,label4;
    JButton loginButton,exitButton;
    JTextField jTextField1,jTextField2,jTextField3;

    public Triangle() {
        this.setTitle("面积");
        this.setSize(250,200);
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        panel = new JPanel();
        panel.setLayout(new FlowLayout());//设置为流式布局
        label = new JLabel("边1");
        label2 = new JLabel("边2");
        label3 = new JLabel("边3");
        label4 = new JLabel("");
        jTextField1 = new JTextField(16);//设置文本框的长度
        jTextField2 = new JTextField(16);//设置文本框的长度
        jTextField3 = new JTextField(16);//设置文本框的长度
        loginButton = new JButton("计算");
        loginButton.addActionListener(new TriangleActionEvent(this));//监听事件
        exitButton = new JButton("清除");
        exitButton.addActionListener(new TriangleActionEvent(this));//监听事件



        panel.add(label);//把组件添加到面板panel
        panel.add(jTextField1);
        panel.add(label2);
        panel.add(jTextField2);
        panel.add(label3);
        panel.add(jTextField3);
        panel.add(loginButton);
        panel.add(exitButton);
        panel.add(label4);
        this.add(panel);//实现面板panel

        this.setVisible(true);//设置可见
    }

}




