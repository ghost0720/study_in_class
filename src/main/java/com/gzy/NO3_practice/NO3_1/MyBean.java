package com.gzy.NO3_practice.NO3_1;

import java.io.Serializable;

public class MyBean implements Serializable {
    private int[] num;

    private int he;
    private double pjs;
    private int max;
    private int min;

    public int[] getNum() {
        return num;
    }

    public void setNum(int[] num) {
        this.num = num;
    }

    public int getHe() {
        return he;
    }

    public void setHe(int he) {
        this.he = he;
    }

    public double getPjs() {
        return pjs;
    }

    public void setPjs(double pjs) {
        this.pjs = pjs;
    }

    public int getMax() {
        return max;
    }

    public void setMax(int max) {
        this.max = max;
    }

    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }
}
