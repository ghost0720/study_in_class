package com.gzy.NO3_practice;

import jdk.internal.org.objectweb.asm.tree.MultiANewArrayInsnNode;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class NO10 {

    private static int[] sort = new int[720];
    private static int j = 0;

    public static void main(String[] args) {

        //定义49个数
        int[] ints = new int[10];
        for (int i = 0; i < 10; i++) {
            ints[i] = i;
        }

        //从中抽取6个数放到array数组中
        Integer[] array = new Integer[6];
        for (int i = 0; i < 6; i++) {
            int j = (int)(Math.random()*10);
            array[i] = ints[j];
        }


        //得到抽到的6个数的全排列，并放入到sort数组中
        List<Integer> list = Arrays.asList(array);
        DFS(list,"");

        //对sort数组进行基数排序
        redixSort(sort);

        //输出从小到大的数
        System.out.println("排序后");
        for (int i = 0; i < sort.length; i++) {
            System.out.println(sort[i]);
        }

    }

    public static void DFS(List<Integer> candidate,String prefix){

        if(prefix.length()!=0 && candidate.size()==0){
            sort[j] = Integer.valueOf(prefix);
            j++;
        }


        for(int i=0; i<candidate.size(); i++){

            List<Integer> temp = new LinkedList<Integer>(candidate);
            int item = (int)temp.remove(i);  // 取出被删除的元素，这个元素当作一个组合用掉了
            DFS(temp, prefix+item);

        }
    }


    //基数排序算法
    public static void redixSort(int[] arr) {

        //定义桶
        int[][] bucket = new int[10][arr.length];

        //定义一个一维数组，来记录每个桶的数据个数
        int[] bucketElementCounts = new int[10];

        int wei = 1;
        int temp = 0;

        while (temp != arr.length) {


            for (int i = 0; i < arr.length; i++) {
                int digitOfElement = (arr[i] / wei) % 10;//取位
                //放入桶里面
                bucket[digitOfElement][bucketElementCounts[digitOfElement]] = arr[i];
                bucketElementCounts[digitOfElement]++;
            }
            temp = bucketElementCounts[0];

            //取出桶中数据,放入到原数组里面
            int index = 0;
            for (int i = 0; i < bucketElementCounts.length; i++) {
                for (int j = 0; j < bucketElementCounts[i]; j++) {
                    if (bucketElementCounts[i] != 0) {
                        arr[index] = bucket[i][j];
                        index++;
                    }
                }
                bucketElementCounts[i] = 0;
            }

            wei = wei * 10;

        }
    }



}
