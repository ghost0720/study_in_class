package com.gzy.NO3_practice.NO3_2;


import java.io.Serializable;
import java.time.LocalDate;

/**
 * <p><strong>Employee</strong>雇员类，用来处理单个员工的信息</p>
 * @version 1.0
 * @author 190207101guojiping
 */
public class Employee implements Serializable {

    private static int nextId;  //用来统一自动计算每一个新增员工的工号
    private int id;//员工的工号
    private String name="";//员工名字
    private LocalDate hireDay;//员工入职时间
    private double salary;//员工基本工资
    private String position="";//员工所属部门、职位描述
    private String tel;//电话号码

    Employee(){

    }
    /**
     * @param id 工号
     * @param n  名字
     * @param y  入职年
     * @param m  入职月
     * @param d  入职日
     * @param s  每月基本工资
     * @param pos 所属部门职位描述
     * @param tel  手机号
     */
    Employee(int id , String n , int y , int m , int d , double s , String pos , String tel  ){

    }

    Employee(int id , String n , LocalDate localDate, double s , String pos , String tel  ){
        this.id = id;
        this.name = n;
        this.hireDay = localDate;
        this.salary = s;
        this.position = pos;
        this.tel = tel;
    }

    /**
     * 该函数为增加员工的年底工资
     * @param percent  年底工资增长比例
     * @return  返回增加后的工资
     */
    double  raiseSalary(double percent) {
        return 0.0;
    }

    /**
     * 该函数根据当前时间计算员工的入职年限
     * @return 返回入职年限，按整数计算
     */
    int   getHireTime() {
        return 0;
    }

    /**
     * 显示自己的信息
     */
    void  showEmployeeMsg() {
        System.out.println(this);
    }

    //以下是每种属性的get和set函数，可以在eclipse中自动生成
    public static int getNextId() {
        return nextId;
    }
    public static void setNextId(int nextId) {
        Employee.nextId = nextId;
    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public LocalDate getHireDay() {
        return hireDay;
    }
    public void setHireDay(LocalDate hireDay) {
        this.hireDay = hireDay;
    }
    public double getSalary() {
        return salary;
    }
    public void setSalary(double salary) {
        this.salary = salary;
    }
    public String getPosition() {
        return position;
    }
    public void setPosition(String position) {
        this.position = position;
    }
    public String getTel() {
        return tel;
    }
    public void setTel(String tel) {
        this.tel = tel;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", hireDay=" + hireDay +
                ", salary=" + salary +
                ", position='" + position + '\'' +
                ", tel='" + tel + '\'' +
                '}';
    }
}
