package com.gzy.NO3_practice.NO3_2;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class system_1 {

    private static Scanner scan; //由于很多方法都需要键盘输入数据，所以将scan对象放在类成员使用
    private static Corporation corporation;
    public static void main(String[] args) {
        scan = new Scanner(System.in);
        // TODO Auto-generated method stub
        corporation= new Corporation("郭泽宇帅比有限公司", "专门从事描写郭泽宇是多么帅的公司","郭泽宇");

        //假数据
        Employee[] employees = corporation.getEmployees();
        Employee employee1 = new Employee(1, "ayu1", LocalDate.now(), 1000, "a", "a");
        Employee employee2 = new Employee(2, "ayu2", LocalDate.now(), 4000, "b", "v");
        Employee employee3 = new Employee(3, "ayu3", LocalDate.now(), 2000, "c", "d");
        Employee employee4 = new Employee(4, "ayu4", LocalDate.now(), 3000, "d", "ac");
        employees[0] = employee1;
        employees[1] = employee2;
        employees[2] = employee3;
        employees[3] = employee4;
        //把假数据加入到corporation里面
        corporation.setEmployees(employees);
        corporation.setEmployeeNumber(4);

        //创建一个对象
        system_1 system_1 = new system_1();
        //显示主界面，并等待输入菜单选项，并执行所选功能
        system_1.waitforMenuSelect();



    }

    /**
     * 显示公司工资管理主界面
     */
    public  void  showMainUI() {
        System.out.println("***************************************");
        System.out.printf("***********%s职员工资管理系统\n" , corporation.getCorpName());
        System.out.println("输入add:添加职员信息");
        System.out.println("输入del:删除职员信息");
        System.out.println("输入search:查询职员信息");
        System.out.println("输入modify:修改职员信息");
        System.out.println("输入sort:对职员进行排序");
        System.out.println("输入save:保存职员信息");
        System.out.println("输入save as:另存为...职员信息");
        System.out.println("输入open:打开文件读入职员信息");
        System.out.println("输入show:显示所有职员信息");
        System.out.println("输入exit:退出系统");
        System.out.printf("***********%s****总经理:%s\n" , corporation.getCorpDescript() , corporation.getSuperManager());
        System.out.println("***********************************************");
    }

    /**
     * 等待用户选择操作选项，比如：键盘输入"add"，则选择添加一职员功能
     */
    public void waitforMenuSelect() {

        String select="";
        //使用ret布尔值来控制
        Boolean ret = true;
        while(ret) {
            //展示主页面
            showMainUI();
            //输入一行字符
            select = scan.nextLine();

            switch(select) {
                case "add":addOneEmployee();break;
                case "del":delOneEmployee();break;
                case "search":searchEmployee();break;
                case "modify":modifyEmployeeMsg();break;
                case "sort":sortEmployee();break;
                case  "show":showAllEmployees();break;
                case "exit":ret = false;break;
                case "save":saveCorporationToLocal();break;
                case "saveAs":saveAsCorporationToLocal();break;
                case "open":openCorporationInLocal();break;
                default:break;
            }


        }

    }

    /**
     * 将文件读取到corporation对象中显示
     */
    private void openCorporationInLocal() {
        System.out.print("请输入你要打开文件的路径:");
        String path = scan.nextLine();
        //读取文件中内容转为对象
        Corporation cor = ObjectDatUtil.dat2Object(path, Corporation.class);
        //把读取出来的对象直接赋值给corporation对象
        corporation = cor;
        //输出信息
        System.out.println("打开成功，当前公司为打开的公司信息");
    }

    /**
     * 另存为方法
     * 保存当前corporation对象数据到本地文件.dat
     */
    private void saveAsCorporationToLocal() {
        System.out.print("请输入你要保存的路径(文件名要加上.dat的后缀):");
        //输入要保存的文件目录
        String path = scan.nextLine();
        try {
            //将对象保存到输入的文件目录的地方
            ObjectDatUtil.object2Dat(corporation,path);
            System.out.println("保存成功");//提示信息
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //保存
    private void saveCorporationToLocal() {
        //默认保存地址
        System.out.println("默认D://a.dat");
        String path = "D://a.dat";
        try {
            //将对象保存到输出的文件目录的地方
            ObjectDatUtil.object2Dat(corporation,path);
            System.out.println("保存成功");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 添加一职员（从键盘输入基本信息）
     * @return 返回目前公司的当前职员数
     */
    public void  addOneEmployee() {
///**
//     * @param id 工号
//     * @param n  名字
//     * @param s  每月基本工资
//     * @param pos 所属部门职位描述
//     * @param tel  手机号
//     */
//    Employee(int id , String n , int y , int m , int d , double s , String pos , String tel  ){
//
//    }
        System.out.println("进入增加一职员界面,请按格式输入数据(id,name,salary,position,tel)(中间用空格分隔)");
        //具体用scan来解析输入的数据
        String s = scan.nextLine();

        //将输入的数据按照空格分开，然后将每个数据创建为一个对象
        String[] info = s.split(" ");
        Employee employee = new Employee(Integer.valueOf(info[0]), info[1], LocalDate.now(), Double.valueOf(info[2]), info[3], info[4]);

        /*
        方法1
        Employee[] employees = corporation.getEmployees();
        Employee[] employees1;
        if (employees[0]==null) {
            employees1 = new Employee[1];
            employees1[0] = employee;
        }else {
            employees1 = new Employee[employees.length + 1];
            for (int i = 0; i < employees.length; i++) {
                employees1[i] = employees[i];
            }
            corporation.setEmployeeNumber(corporation.getEmployeeNumber()+1);
            employees1[employees.length] = employee;
        }
        corporation.setEmployees(employees1);*/

        //方法2
        //得到当前公司的职工数
        int employeeNumber = corporation.getEmployeeNumber();
        //判断该公司的职工数是否到达了一百人
        if(employeeNumber>=100) {
            System.out.println("当前公司职工数已经到达100，不能再添加了");
            return ;
        }
        //得到当前公司的职工数组
        Employee[] employees = corporation.getEmployees();
        //将输入进来的职工保存到该数组中
        employees[employeeNumber] = employee;
        //然后将数组再次保存到当前公司中
        corporation.setEmployees(employees);
        //把当前公司的职工数加一
        corporation.setEmployeeNumber(corporation.getEmployeeNumber()+1);


        System.out.println("添加成功");
    }




    /**
     * 删除一职员,从键盘输入要删除职员的工号
     *  @return  目前公司的当前职员数
     */
    public void delOneEmployee() {

        System.out.println("进入删除一职员界面,请按格式输入工号数据");
        //具体用scan来解析输入的数据
        int id = scan.nextInt();
        scan.nextLine();//接收后面那个sb\n
        Employee employee = null;//记录该职工信息
        int j = -1;//
        Employee[] employees = corporation.getEmployees();
        for (int i = 0; i < employees.length; i++) {
            if (id==employees[i].getId()) {
                employee = employees[i];
                j = i;
                break;
            }
        }
        if (employee==null) {
            System.out.println("没有该id的员工");
            return ;
        }
        int employeeNumber = corporation.getEmployeeNumber()-1;

        corporation.setEmployeeNumber(employeeNumber);
        Employee employee1 = employees[employeeNumber];//最后一个雇员
        //将最后一个雇员删除，补到要删除的雇员的位置
        employees[j] = employee1;
        employees[employeeNumber] = null;
        corporation.setEmployees(employees);

        System.out.println("删除数据成功");
    }

    /**
     * 查找职员信息的菜单入口
     */
    public void searchEmployee() {

        System.out.println("进入查找职员界面,请输入选项，输入id则按id查找数据，输入name则按名字查找数据");
        //具体用scan来解析输入的数据
        String select = scan.nextLine();
        if(select.equals("id")){
            System.out.println("请输入工号，按回车键结束");
            int id = scan.nextInt();
            scan.nextLine();//接收后面那个\n傻逼数据，操
            Employee find = searchEmployeeById(id);
            if(find == null)
                System.out.println("没有找到信息");
            else
                find.showEmployeeMsg();

        }else {
            System.out.println("请输入名字，按回车键结束");
            String name = scan.nextLine();
            Employee[] finds = searchEmployeesByName(name);
            int len = finds.length;
            if(len ==0)
                System.out.println("没有找到信息");
            else
                for(Employee e :finds)
                    e.showEmployeeMsg();
        }

    }

    /**
     * 根据工号查找职员信息
     * @return  查找到的职员信息
     */
    public  Employee searchEmployeeById(int id) {

        for (Employee employee:corporation.getEmployees()) {
            if (id==employee.getId()) {
                return employee;
            }
        }

        return null;
    }
    /**
     * 根据职员姓名查找职员信息
     * @param name   姓名
     * @return  查找到的多个职员信息，因为有可能重名
     */
    public Employee[] searchEmployeesByName(String name) {
        List<Employee> list = new LinkedList<>();
        int i = 0;
        for (Employee employee:corporation.getEmployees()) {
            if (employee.getName().equals(name)) {
                list.add(employee);
                i += 1;
            }
        }
        Employee[] employees = new Employee[i];
        i = 0;
        for (Employee employee:list) {
            employees[i] = employee;
            i += 1;
        }
        return employees;
    }
    /**
     * 修改某个职员的基本信息
     * 从键盘输入工号，修改该雇员的基本信息
     * @return   修改信息后的职员数据
     */
    public Employee modifyEmployeeMsg() {
        System.out.print("输入职工信息（通过id来确认要修改的职工）:");
        String s = scan.nextLine();
        String[] info = s.split(" ");
        Employee employeeUpdate = new Employee(Integer.valueOf(info[0]), info[1], LocalDate.now(), Double.valueOf(info[2]), info[3], info[4]);
        Employee[] employees = corporation.getEmployees();
        boolean b = false;
        for (int i=0;i<employees.length;i++) {
            if (employees[i].getId()==Integer.valueOf(info[0])) {
                b = true;
                LocalDate hireDay = employees[i].getHireDay();
                employeeUpdate.setHireDay(hireDay);
                employees[i] = employeeUpdate;
            }
        }
        if (b) {
            System.out.println("修改信息成功");
        }else {
            System.out.println("未找到该id的职工");
        }

        return employeeUpdate;
    }
    /**
     * 排序的总入口
     */
    public void  sortEmployee() {
//        System.out.println("对不起，你要的排序暂不提供此功能，请稍后尝试");
        System.out.println("按照薪酬排序:");
        sortEmployeeBySalary();
    }
    /**
     * 根据薪资高低排序
     */
    public void  sortEmployeeBySalary() {
        Employee employee = null;
        Employee[] employees = corporation.getEmployees();
        int employeeNumber = corporation.getEmployeeNumber();
        if (employees.length<2) {
            return ;
        }

        for (int i = 0; i < employeeNumber; i++) {
            for (int j = i; j < employeeNumber; j++) {
                //把最大的放前面
                if (employees[i].getSalary()<employees[j].getSalary()) {
                    employee = employees[j];
                    employees[j] = employees[i];
                    employees[i] = employee;
                }
            }
        }

        corporation.setEmployees(employees);
        showAllEmployees();

    }

    /**
     * 根据入职时间长短排序
     */
    public void sortEmployeeByHireTime() {

    }

    /**
     * 显示所有的雇员信息
     */
    public void showAllEmployees() {
        for (Employee employee:corporation.getEmployees()) {
            if (employee==null) {
                return ;
            }
            System.out.println(employee);
        }
    }

    /**
     * 保存雇员信息到文件
     * @param fileName
     */
    public void saveEmployeesToFile(String fileName) {

    }
    /**
     * 另存雇员信息到所选择文件
     */
    public  void savaAsEmployeesToFile() {

    }

    /**
     * 打开某文件，读入雇员信息
     * @param fileName
     * @return
     */
    public Boolean openFile(String fileName) {
        return true;
    }

    /**
     * 打开所选文件，读入雇员信息
     * @return
     */
    public Boolean openFile() {
        return true;
    }

    /**
     * 计算按某个比例增加员工工资后的总工资
     */
    public double calculateAllSalary(double percent) {
        return 0.0;
    }

}
