package com.gzy.NO3_practice.NO3_2;

import java.io.Serializable;

/**
 * <p><strong>Employee</strong>公司类，用来处理所有单个员工的信息</p>
 * 一般来说，管理系统中有原子类如Employee，还有集合类如Corpration
 * @version 1.0
 * @author 190207101guojiping
 */
public class Corporation implements Serializable {

	public static final int  MAX_EMPLOYEES = 100;  //公司员工的最大数

	private String corpName;  //公司名
	private String corpDescript;  //公司业务描述
	private String superManager;  //公司法人

	private  int   employeeNumber; //公司实际员工数
	private  Employee[]  employees; //用来存放公司的员工对象





	Corporation(String name , String des , String superManager){
		corpName = name;
		corpDescript = des;
		this.superManager = superManager;
		employees = new Employee[MAX_EMPLOYEES];
		employeeNumber = 0;

	}



	public String getCorpName() {
		return corpName;
	}


	public void setCorpName(String corpName) {
		this.corpName = corpName;
	}


	public String getCorpDescript() {
		return corpDescript;
	}


	public void setCorpDescript(String corpDescript) {
		this.corpDescript = corpDescript;
	}


	public String getSuperManager() {
		return superManager;
	}


	public void setSuperManager(String superManager) {
		this.superManager = superManager;
	}


	public int getEmployeeNumber() {
		return employeeNumber;
	}


	public void setEmployeeNumber(int employeeNumber) {
		this.employeeNumber = employeeNumber;
	}


	public Employee[] getEmployees() {
		return employees;
	}


	public void setEmployees(Employee[] employees) {
		this.employees = employees;
	}


	public static int getMaxEmployees() {
		return MAX_EMPLOYEES;
	}



}
