package com.gzy.NO3_practice;

import java.util.Scanner;

public class NO4 {

    private static Salary[] salarys;
    public static void main(String[] args) {

        boolean b = true;
        Salary salary1 = new Salary(1, 2000);
        Salary salary2 = new Salary(2, 4000);
        Salary salary3 = new Salary(3, 5000);
        Salary salary4 = new Salary(4, 6000);
        salarys = new Salary[]{salary1, salary2, salary3, salary4};
        while(b) {
            System.out.println("查询工资(1)");
            System.out.println("添加职工信息(2)");
            System.out.println("修改职工工资(3)");
            System.out.println("查询所有员工工资(4)");
            System.out.println("退出系统(9)");
            Scanner scanner = new Scanner(System.in);
            int str = scanner.nextInt();

            switch (str) {
                case 1 :
                    System.out.print("请输入员工id:");
                    int id = scanner.nextInt();
                    System.out.println(getSalaryById(id));
                    break;
                case 2 :
                    System.out.print("输入添加员工id:");
                    int idByAdd = scanner.nextInt();
                    System.out.print("输入添加的工资:");
                    double salary = scanner.nextDouble();
                    addSalaryTosalary(idByAdd,salary);

                    break;
                case 3 :
                    System.out.print("输入要修改员工id:");
                    int idByUpdate = scanner.nextInt();
                    System.out.print("输入要修改的工资:");
                    double salaryByUpdate = scanner.nextDouble();
                    updateSalaryById(idByUpdate,salaryByUpdate);
                    System.out.println("修改成功");
                    break;
                case 4 :
                    for (Salary sa:salarys) {
                        System.out.println(sa);
                    }
                    break;
                case 9 :
                    b = false;
                default: break;

            }
        }

    }

    private static void updateSalaryById(int idByUpdate, double salaryByUpdate) {
        for (Salary s:salarys) {
            int id1 = s.getId();
            if (idByUpdate==id1) {
                s.setSalary(salaryByUpdate);
                break;
            }
        }
    }


    //添加数据到salary数组
    private static void addSalaryTosalary(int idByAdd, double sa) {
        Salary[] salaries = new Salary[salarys.length + 1];
        for (int i = 0; i < salarys.length; i++) {
            if (idByAdd == salarys[i].getId()) {
                System.out.println("id重复");
                return;
            }
            salaries[i] = salarys[i];
        }
        Salary salar = new Salary(idByAdd, sa);
        salaries[salarys.length] = salar;
        salarys = salaries;
        System.out.println("添加成功");
    }

    //通过id查询员工信息
    public static Salary getSalaryById(int id) {
        Salary return_salary = null;
        for (Salary s:salarys) {
            int id1 = s.getId();
            if (id==id1) {
                return_salary = s;
                break;
            }
        }
        return return_salary;
    }

}

class Salary {

    private int id;
    private double salary;

    public Salary(int id, double salary) {
        this.id = id;
        this.salary = salary;
    }

    public Salary() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    @Override
    public String toString() {
        return "salary{" +
                "id=" + id +
                ", salary=" + salary +
                '}';
    }
}
