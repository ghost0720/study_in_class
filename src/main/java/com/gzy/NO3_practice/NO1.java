package com.gzy.NO3_practice;


import java.util.Scanner;

/**
 题目：企业发放的奖金根据利润提成。利润(I)低于或等于10万元时，奖金可提10%；
 利润高于10万元，低于20万元时，低于10万元的部分按10%提成，高于10万元的部分，可
 可提成7.5%；20万到40万之间时，高于20万元的部分，可提成5%；40万到60万之间时
 高于40万元的部分，可提成3%；60万到100万之间时，高于60万元的部分，可提成1.5%
 ，高于100万元时，超过100万元的部分按1%提成，从键盘输入当月利润I，求应发放
 奖金总数？
 */
public class NO1 {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        //输入一个数字，单位万元
        int i = scanner.nextInt();
        //奖金总数
        double sum = 0;
        if (i<=10) {
            sum = sum*1.1;
        }else if (i<20) {
            sum = 10*1.1 + (i-10)*1.075;
        }else if (i<40) {
            sum = 10*1.1 + 10*1.075 + (i-20)*1.05;
        }else if (i<60) {
            sum = 10*1.1 + 10*1.075 + 20*1.05 + (i-40)*1.03;
        }else if(i<100) {
            sum = 10*1.1 + 10*1.075 + 20*1.05 + 20*1.03 + (i-60)*1.015;
        }else {
            sum = 10*1.1 + 10*1.075 + 20*1.05 + 20*1.03 + 40*1.015 + (i-100)*1.01;
        }

        System.out.println(sum);



    }

}
