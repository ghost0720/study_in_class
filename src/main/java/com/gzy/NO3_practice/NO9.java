package com.gzy.NO3_practice;

import java.util.Stack;

public class NO9 {
    static int j = 1;

    public static class Main {


        public static Stack<Integer> stack = new Stack<Integer>();
        public static void main(String[] args) {
            String password = "ghost0720";
            String sourceString = "123456789dsgghadf8546878548";

            int[] ch = jiami(password, sourceString);
            for (int i = 0; i < ch.length; i++) {
                System.out.print(ch[i] + "  ");
            }

            System.out.println();

            String s = jiemi(password,ch);
            System.out.println(s);
        }

        private static String jiemi(String password, int[] ch) {

            char[] p = password.toCharArray();
            char[] s = new char[ch.length];
            int current = 0;
            for (int i = 0; i < ch.length; i++) {
                if (current>=p.length) {
                    current = 0;
                }
                int pi = Integer.valueOf(p[current]);
                s[i] = (char)(ch[i] - pi);
                current++;
            }
            String str = "";
            for (int i = 0; i < s.length; i++) {
                str = str + s[i];
            }
            return str;
        }


    }

    public static int[] jiami(String password,String sourceString) {
        char[] p = password.toCharArray();
        char[] s = sourceString.toCharArray();

        int[] ch = new int[s.length];
        int current = 0;
        for (int i = 0; i < s.length; i++) {
            if (current>=p.length) {
                current = 0;
            }

            ch[i] = Integer.valueOf(s[i]+p[current]);
            current++;
        }



        return ch;
    }

}
