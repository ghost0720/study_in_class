package com.gzy.NO3_practice;

import java.util.Scanner;

public class NO11 {

    public static void main(String[] args) {

        int[] scores = new int[6];
        int sum = 0;
        Scanner scanner = new Scanner(System.in);
        int size = 0;
        while(size<6) {

            int i = scanner.nextInt();
            if (i>100&&i<0) {
                System.out.println("输入数据不合法，重新输入");
                continue;
            }
            sum = sum + i;
            scores[size] = i;

            size++;
        }

        sum = sum - getMax(scores) - getMin(scores);

        System.out.println(sum/4);


    }

    //得到数组中最大的数
    public static int getMax(int[] scores) {
        int max = scores[0];
        for (int i = 1; i < scores.length; i++) {
            if (max < scores[i]) {
                max = scores[i];
            }
         }
        return max;
    }

    //得到数组中最小的数
    public static int getMin(int[] scores) {
        int min = scores[0];
        for (int i = 1; i < scores.length; i++) {
            if (min > scores[i]) {
                min = scores[i];
            }
        }
        return min;
    }

}
