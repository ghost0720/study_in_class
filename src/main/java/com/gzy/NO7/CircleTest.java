package com.gzy.NO7;

import java.util.Scanner;

public class CircleTest {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Circle c = new Circle();

        System.out.println("请输入圆的半径:");
        c.setR(sc.nextDouble());

        System.out.println("请输入圆柱的高:");
        c.setH(sc.nextDouble());

        System.out.printf("圆柱的面积为:"+ c.getArea());
        System.out.printf("圆柱的体积为:"+ c.getVolume());
    }
}
