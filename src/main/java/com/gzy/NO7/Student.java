package com.gzy.NO7;

public class Student extends Person {

	int total;

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public Student(int total) {
		super();
		this.total = total;
	}

	@Override
	public String toString() {
		return "Student [total=" + total + "]";
	}
	
	public Student(int w,int h,int t) {
		this.hight = h;
		this.weight = w;
		this.total = t;
	}
	
}
