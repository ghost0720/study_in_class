package com.gzy.NO7;

public class Cilent {
	private int cardId;
	private int grade;
	private String name;
	private int integral;
	private int age;
	private String sex;
	
	public Cilent(int cardId, int grade, String name,int integral, int age,String sex) {
		super();
		this.cardId = cardId;
		this.grade = grade;
		this.name = name;
		this.integral = integral;
		this.age = age;
		this.sex = sex;
	}
	public Cilent() {
		super();
	}
	
	
	public int getCardId() {
		return cardId;
	}
	public void setCardId(int cardId) {
		this.cardId = cardId;
	}
	public int getGrade() {
		return grade;
	}
	public void setGrade(int grade) {
		this.grade = grade;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getIntegral() {
		return integral;
	}
	public void setIntegral(int integral) {
		this.integral = integral;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	@Override
	public String toString() {
		return "Cilent [VIP卡号=" + cardId + ", VIP等级=" + grade + ", 姓名=" + name + ", 积分=" +integral
				+ ", 年龄=" + age + ", 性别=" + sex + "]";
	}



}
