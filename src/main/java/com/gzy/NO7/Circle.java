package com.gzy.NO7;

import java.util.Scanner;

class Circle{
    public static final double PI=3.14;

    private double r;
    private double h;

    public Circle() {
        super();
    }

    public Circle(double r, double h) {
        super();
        this.r = r;
        this.h = h;
    }

    public double getR() {
        return r;
    }

    public void setR(double r) {
        this.r = r;
    }

    public double getH() {
        return h;
    }

    public void setH(Double h) {
        this.h = h;
    }

    public static double getPi() {
        return PI;
    }

    public double getArea(){
        return PI * r * r * 2 + h * 2 * PI * r;
    }

    public double getVolume(){
        return PI * r * r * h;
    }



}


