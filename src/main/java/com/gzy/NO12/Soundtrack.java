package com.gzy.NO12;

public class Soundtrack {

    private Music music = null;
    private int seconds;//持续时间
    private int startPosition;//开始位置

    public Soundtrack(Music music, int seconds, int startPosition) {
        this.music = music;
        this.seconds = seconds;
        this.startPosition = startPosition;
    }

    public Soundtrack() {
        music = null;
    }


    @Override
    public String toString() {
        return "Soundtrack{" +
                "music=" + music +
                ", seconds=" + seconds +
                ", startPosition=" + startPosition +
                '}';
    }

    public Music getMusic() {
        return music;
    }

    public void setMusic(Music music) {
        this.music = music;
    }

    public int getSeconds() {
        return seconds;
    }

    public void setSeconds(int seconds) {
        this.seconds = seconds;
    }

    public int getStartPosition() {
        return startPosition;
    }

    public void setStartPosition(int startPosition) {
        this.startPosition = startPosition;
    }
}
