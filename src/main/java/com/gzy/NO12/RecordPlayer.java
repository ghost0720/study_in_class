package com.gzy.NO12;

import java.util.*;
import java.util.stream.Collectors;

public class RecordPlayer extends Thread {

    private static int MAXELECTRICITY = 100;

    private SingPage singPage;
    private int lastElectricity;//剩余电量


    public void play() {
        System.out.println("播放" + singPage);

        Map<Integer, Soundtrack> soundtracks = singPage.getSoundtracks();

        Set<Integer> keys = soundtracks.keySet();

        List<Integer> collect = keys.stream().sorted(Comparator.naturalOrder()).collect(Collectors.toList());

        for (Integer i:collect) {
            System.out.print("播放：");
            System.out.println(soundtracks.get(i));
            try {
                Thread.sleep(soundtracks.get(i).getSeconds()*1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }


    }

    public void connectionComputer() {
        System.out.println("连接电脑");

        //刻录
        reCord();
        addElectricity();

    }

    private void addElectricity() {
        while(lastElectricity<MAXELECTRICITY) {
            lastElectricity++;
            try {
                Thread.sleep(1000);
                System.out.println("电量充电中，剩余" + lastElectricity);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("充电完成");
    }

    private void reCord() {
        System.out.println("把唱片刻录到电脑上");
    }

    public RecordPlayer(SingPage singPage, int lastElectricity) {
        this.singPage = singPage;
        this.lastElectricity = lastElectricity;
    }

    @Override
    public void run() {
        this.play();
    }
}
