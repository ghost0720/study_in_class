package com.gzy.NO12;

import com.gzy.NO12.Singer.SingleSinger;

import java.util.HashMap;

public class text11 {

    public static void main(String[] args) {

        SingleSinger jay = new SingleSinger("周杰伦",20,"男",null);
        Soundtrack soundtrack = new Soundtrack(new Music("七里香"),3,4);
        Soundtrack soundtrack1 = new Soundtrack(new Music("七香"),2,1);
        Soundtrack soundtrack2 = new Soundtrack(new Music("香"),5,2);
        Soundtrack soundtrack3 = new Soundtrack(new Music("香"),100,5);
        HashMap<Integer, Soundtrack> soundtracks = new HashMap<>();
        soundtracks.put(soundtrack.getStartPosition(),soundtrack);
        soundtracks.put(soundtrack1.getStartPosition(),soundtrack1);
        soundtracks.put(soundtrack2.getStartPosition(),soundtrack2);

        RecordPlayer recordPlayer = new RecordPlayer(new SingPage("唱片名", jay, "周文山", soundtracks), 1);

        recordPlayer.start();
        recordPlayer.connectionComputer();

    }

}
