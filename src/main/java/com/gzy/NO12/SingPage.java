package com.gzy.NO12;

import com.gzy.NO12.Singer.Singer;

import java.util.Arrays;
import java.util.HashMap;

public class SingPage {

    private String name;
    private Singer singer;
    private String writer;

    private HashMap<Integer,Soundtrack> soundtracks;

    public SingPage(String name, Singer singer, String writer, HashMap<Integer,Soundtrack> soundtracks) {
        this.name = name;
        this.singer = singer;
        this.writer = writer;
        this.soundtracks = soundtracks;
    }

    @Override
    public String toString() {
        return "SingPage{" +
                "name='" + name + '\'' +
                ", singer=" + singer +
                ", writer='" + writer + '\'' +
                ", soundtracks=" + soundtracks +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Singer getSinger() {
        return singer;
    }

    public void setSinger(Singer singer) {
        this.singer = singer;
    }

    public String getWriter() {
        return writer;
    }

    public void setWriter(String writer) {
        this.writer = writer;
    }

    public HashMap<Integer, Soundtrack> getSoundtracks() {
        return soundtracks;
    }

    public void setSoundtracks(HashMap<Integer, Soundtrack> soundtracks) {
        this.soundtracks = soundtracks;
    }
}
