package com.gzy.NO12;

public class Music {

    private String name;

    public Music(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Music{" +
                "name='" + name + '\'' +
                '}';
    }
}
