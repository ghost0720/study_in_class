package com.gzy.NO12.Singer;

public class SingleSinger implements Singer {

    private String name;
    private int age;
    private String gender;
    private GroupSinger groupSinger = null;

    public SingleSinger(String name, int age, String gender, GroupSinger groupSinger) {
        this.name = name;
        this.age = age;
        this.gender = gender;
        this.groupSinger = groupSinger;
    }

    @Override
    public String toString() {
        return "SingleSinger{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", gender='" + gender + '\'' +
                ", groupSinger=" + groupSinger +
                '}';
    }
}
