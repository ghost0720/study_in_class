package com.gzy.NO12.Singer;

import java.util.Arrays;

public class GroupSinger implements Singer {

    private String groupName;
    private String[] groupMember;
    private int age;


    public GroupSinger(String groupName, String[] groupMember, int age) {
        this.groupName = groupName;
        this.groupMember = groupMember;
        this.age = age;
    }


    @Override
    public String toString() {
        return "GroupSinger{" +
                "groupName='" + groupName + '\'' +
                ", groupMember=" + Arrays.toString(groupMember) +
                ", age=" + age +
                '}';
    }
}
