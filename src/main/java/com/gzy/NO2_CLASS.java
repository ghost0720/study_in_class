package com.gzy;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class NO2_CLASS {

    public static void main(String[] args) {

        String s = "hello world";
        System.out.println(s);

        for (int i = 0; i < s.length() * 2; i++) {
            System.out.print("=");
        }
        System.out.println();

        stu stu = new stu();
        stu.p();

    }

}

class stu {

    public void p() {
        //
        String s = "\\u4f60\\u662f\\u50bb\\u903c\\u5417\\uff1f\\u5929\\u5929\\u8f93\\u51fa\\u4e0a\\u9762\\u90a3\\u53e5\\u8bdd";
        System.out.println(unicodeDecode(s));
    }

    /**
     * @Title: unicodeDecode
     * @Description: unicode解码
     * @return
     */
    public static String unicodeDecode(String string) {
        Pattern pattern = Pattern.compile("(\\\\u(\\p{XDigit}{4}))");
        Matcher matcher = pattern.matcher(string);
        char ch;
        while (matcher.find()) {
            ch = (char) Integer.parseInt(matcher.group(2), 16);
            string = string.replace(matcher.group(1), ch + "");
        }
        return string;
    }

}
